//
//  FlickrFullVC.swift
//  Swiftr
//
//  Created by Edward Arenberg on 8/19/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

import UIKit

class FlickrFullVC: UIViewController {
    
    // URL for large size image
    var url : String? {
        didSet {
            // Get image in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0)) {
                let d = NSData(contentsOfURL: NSURL(string: self.url!))
                self.image = UIImage(data: d)
            }
        }
    }
    // Private property for image
    private var image : UIImage? {
        didSet {
            // Update imageView in main thread
            dispatch_async(dispatch_get_main_queue()) {
                let iv = self.view.viewWithTag(1) as UIImageView
                iv.image = self.image
            }
        }
    }
        
    // Share Action
    @IBAction func shareHit(sender:UIBarButtonItem) {
        var sharingItems = [AnyObject]()
        if let img = image {
            sharingItems.append(img)
        }
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        activityViewController.completionHandler = {(activityType, completed:Bool) in
            if !completed {
                println("Cancelled")
                return
            }
            
            if activityType == UIActivityTypePostToTwitter {
                println("Twitter")
            }
            
            if activityType == UIActivityTypeMail {
                println("Email")
            }
            
            UIAlertView(title: activityType, message: "Completed", delegate: nil, cancelButtonTitle: "OK").show()
        }
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
}
