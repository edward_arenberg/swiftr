//
//  Flickr.swift
//  Swiftr
//
//  Created by Edward Arenberg on 8/19/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

import Foundation

// Delegate protocol definition

protocol FlickrDelegate {
    func gotPics()
    func picError(err:NSError)
}

// Flickr Data Model

class Flickr
{
    // Array of pics which are JSON Dictionaries
    var pics : [NSDictionary]? {
        didSet {
            self.delegate?.gotPics()
        }
    }
    var delegate : FlickrDelegate?  // Optional delegate
    
    init() {
        
    }
    
    private func fetchImages() {
        // Flickr public feed
        let url = NSURL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
        let request = NSURLRequest(URL: url, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 20.0)
        
        // Fetch Flickr feed in background
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) {
            (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            
            if error != nil {
                println("Error: \(error)")
            } else {
                // Hack to fix Flickr JSON format - replace \' with '
                let s = NSString(data: data, encoding: NSUTF8StringEncoding)
                let ss = s.stringByReplacingOccurrencesOfString("\\'", withString: "'")
                let dd = ss.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)
                
                // Decode JSON
                var err : NSError?
                let json = NSJSONSerialization.JSONObjectWithData(dd!, options: .MutableLeaves, error: &err) as? NSDictionary

                if err == nil {
                    // Get list of pic items
                    if let items = json?["items"] as? NSArray {
                        // Set pic list on main thread so U/I can update
                        dispatch_async(dispatch_get_main_queue()) {
                            self.pics = items as? Array<NSDictionary>
                        }
                    }
                } else {
                    // Send error on main thread to show U/I alert
                    dispatch_async(dispatch_get_main_queue()) {
                        let a = 0   // Fix for closure not recognizing next statement
                        self.delegate?.picError(err!)
                    }
                }
            }
        }
    }
    
    func refresh() {
        fetchImages()
    }
    
}