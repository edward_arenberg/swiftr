//
//  FlickrCVC.swift
//  Swiftr
//
//  Created by Edward Arenberg on 8/19/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

import UIKit

let reuseIdentifier = "FlickrCell"

class FlickrCVC: UICollectionViewController, FlickrDelegate {
    
    var flickr : Flickr?
    
    @IBAction func doRefresh(sender:UIBarButtonItem) {
        flickr?.refresh()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        flickr = Flickr()
        flickr!.delegate = self
        flickr!.refresh()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotPics() {
        collectionView.reloadData()
    }
    func picError(err:NSError) {
//        let msg = err.userInfo?["NSDebugDescription"] as String
        UIAlertView(title: "Error", message: err.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
    }

    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView!) -> Int {

        return 1
    }


    override func collectionView(collectionView: UICollectionView!, numberOfItemsInSection section: Int) -> Int {

        return flickr?.pics?.count ?? 0
    }

    override func collectionView(collectionView: UICollectionView!, cellForItemAtIndexPath indexPath: NSIndexPath!) -> UICollectionViewCell! {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as UICollectionViewCell
        
        // Configure the cell
        if let imageView = cell.viewWithTag(1) as? UIImageView {
            imageView.image = UIImage(named: "Flickr160")
            if let m = flickr?.pics?[indexPath.row]["media"] as? NSDictionary {
                if let u = m["m"] as? String {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0)) {
                        let d = NSData(contentsOfURL: NSURL(string: u))
                        let i = UIImage(data: d)
                        dispatch_async(dispatch_get_main_queue()) {
                            imageView.image = i
                        }
                        
                    }
                }
            }
        }
    
        return cell
    }
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        
        
        if let vc = segue.destinationViewController as? FlickrFullVC {
            let paths = collectionView.indexPathsForSelectedItems()
            if let pic = flickr?.pics?[paths[0].row] {
                if let media = pic["media"] as? NSDictionary {
                    if let u = media["m"] as? String {
                        let fullURL = u.stringByReplacingOccurrencesOfString("_m.jpg", withString: "_c.jpg")
                        vc.url = fullURL
                    }
                }
                if let t = pic["title"] as? String {
                    vc.title = t
                }
            }
        }
    }

}
